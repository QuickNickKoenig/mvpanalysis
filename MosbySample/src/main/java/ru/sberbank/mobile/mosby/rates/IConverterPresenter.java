package ru.sberbank.mobile.mosby.rates;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.math.BigDecimal;

import ru.sberbank.mobile.common.rate.entity.Rate;

/**
 * @author QuickNick
 */
public interface IConverterPresenter extends MvpPresenter<IConverterView> {

    void loadRates();

    void tryConvert(BigDecimal sourceAmount, Rate sourceRate, Rate targetRate);
}
