package ru.sberbank.mobile.mosby.person;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.PersonUris;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick
 */

public class DefaultEditOrCreatePersonPresenter extends MvpBasePresenter<IEditOrCreatePersonView>
        implements IEditOrCreatePersonPresenter {

    @Inject
    IPersonsManager mPersonsManager;
    @Inject
    Context mApplicationContext;
    private Person mPerson;

    private ISbolObserverListener mPersonListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData();
        }
    };
    private SbolContentObserver mContentObserver = null;
    private PendingResult<?> mPendingResult;

    public DefaultEditOrCreatePersonPresenter(MosbySampleAppComponent component, Person person) {
        component.inject(this);
        mPerson = person;
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        unregisterObserverSafely();
    }

    @Override
    public Person getPerson() {
        return mPerson;
    }

    @Override
    public void tryEditOrCreatePerson(String name) {
        if (TextUtils.isEmpty(name)) {
            getView().setNameError(true);
        } else {
            getView().setNameError(false);
            mPerson.name = name;
            editOrCreatePerson();
        }
    }

    @Override
    public boolean isUpdatingPerson() {
        return (mPerson.id > 0);
    }

    private void editOrCreatePerson() {
        mPendingResult = isUpdatingPerson() ?
                mPersonsManager.updatePerson(mPerson) : mPersonsManager.addPerson(mPerson);
        Uri uri = mPendingResult.getUri();
        mContentObserver = new SbolContentObserver(mPersonListener);
        mApplicationContext.getContentResolver().registerContentObserver(uri, false, mContentObserver);
        checkData();
    }

    private void checkData() {
        if (isViewAttached()) {
            boolean loading = mPendingResult.isLoading();
            getView().setLoading(loading);
            if (!mPendingResult.isLoading()) {
                mPendingResult.detach();
                getView().onSuccess();
                unregisterObserverSafely();
            }
        }
    }

    private void unregisterObserverSafely() {
        if (mContentObserver != null) {
            mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
            mContentObserver = null;
        }
    }
}
