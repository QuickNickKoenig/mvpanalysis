package ru.sberbank.mobile.mosby.rates;

import android.content.Context;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.math.BigDecimal;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ConverterUtils;
import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.RateUris;
import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick
 */

public class DefaultConverterPresenter extends MvpBasePresenter<IConverterView> implements IConverterPresenter {

    @Inject
    ICurrencyRateManager mCurrencyRateManager;
    @Inject
    Context mApplicationContext;

    private ISbolObserverListener mRatesListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData();
        }
    };
    private SbolContentObserver mContentObserver = new SbolContentObserver(mRatesListener);

    public DefaultConverterPresenter(MosbySampleAppComponent component) {
        component.inject(this);
    }

    @Override
    public void attachView(IConverterView view) {
        super.attachView(view);
        mApplicationContext.getContentResolver().registerContentObserver(RateUris.RATES_BUNDLE_URI,
                false, mContentObserver);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    @Override
    public void loadRates() {
        checkData();
    }

    @Override
    public void tryConvert(BigDecimal sourceAmount, Rate sourceRate, Rate targetRate) {
        if (sourceAmount == null) {
            getView().showSourceAmountError(true);
        } else {
            getView().showSourceAmountError(false);
            if (sourceRate.getCurrency() == targetRate.getCurrency()) {
                getView().showSameRatesError();
            } else {
                convert(sourceAmount, sourceRate, targetRate);
            }
        }
    }

    private void convert(BigDecimal sourceAmount, Rate sourceRate, Rate targetRate) {
        BigDecimal targetAmount = ConverterUtils.convert(sourceAmount, sourceRate, targetRate);
        getView().setTargetAmount(targetAmount);
    }

    private void checkData() {
        if (isViewAttached()) {
            PendingResult<RatesBundle> result = mCurrencyRateManager.getRatesBundle();
            boolean loading = result.isLoading();
            getView().setLoading(loading);
            if (!result.isLoading()) {
                RatesBundle ratesBundle = result.getResult();
                if (ratesBundle.isSuccess()) {
                    getView().setRates(ratesBundle.getRates());
                } else {
                    getView().showServerError(ratesBundle.getConnectorStatus());
                    result.detach();
                }
            }
        }
    }
}
