package ru.sberbank.mobile.mosby.rates;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.ui.RatesAdapter;
import ru.sberbank.mobile.core.network.ConnectorStatus;
import ru.sberbank.mobile.mosby.R;
import ru.sberbank.mobile.mosby.app.BaseActivity;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick.
 */
public class RatesActivity extends BaseActivity<IRatesView, IRatesPresenter>
        implements IRatesView,
        SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private RatesAdapter mAdapter;
    private boolean mFirstLaunch;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, RatesActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirstLaunch = (savedInstanceState == null);

        setContentView(R.layout.recycler_view_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mAdapter = new RatesAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        getPresenter().loadRates(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSwipeRefreshLayout = null;
        mRecyclerView = null;
        mProgressBar = null;
        mAdapter = null;
    }

    @NonNull
    @Override
    public IRatesPresenter createPresenter() {
        return new DefaultRatesPresenter((MosbySampleAppComponent) getComponent());
    }

    // Menu >>>

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rates_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if (!handled) {
            if (item.getItemId() == R.id.converter_menu_item) {
                startActivity(ConverterActivity.newIntent(this));
                handled = true;
            }
        }
        return handled;
    }

    // <<< Menu

    // IRatesView >>>

    @Override
    public void setLoading(boolean loading) {
        mSwipeRefreshLayout.setRefreshing(loading);
        if (loading) {
            if (mFirstLaunch) {
                mSwipeRefreshLayout.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
            }
        } else {
            mFirstLaunch = false;
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setRates(List<Rate> rates) {
        mAdapter.setRates(rates);
    }

    @Override
    public void showServerError(ConnectorStatus status) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.warning)
                .setMessage(status.getTextResId())
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setCancelable(false).show();
    }

    // <<< IRatesView

    // SwipeRefreshLayout.OnRefreshListener >>>

    @Override
    public void onRefresh() {
        getPresenter().loadRates(true);
    }

    // <<< SwipeRefreshLayout.OnRefreshListener
}
