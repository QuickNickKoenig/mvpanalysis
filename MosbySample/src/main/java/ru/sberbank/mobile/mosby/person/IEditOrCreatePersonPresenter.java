package ru.sberbank.mobile.mosby.person;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import ru.sberbank.mobile.common.person.entity.Person;

/**
 * @author QuickNick
 */
public interface IEditOrCreatePersonPresenter extends MvpPresenter<IEditOrCreatePersonView> {

    Person getPerson();

    void tryEditOrCreatePerson(String name);

    boolean isUpdatingPerson();
}
