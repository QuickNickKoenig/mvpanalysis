package ru.sberbank.mobile.mosby.first;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

/**
 * @author QuickNick
 */
public interface IFirstPresenter extends MvpPresenter<IFirstView> {
}
