package ru.sberbank.mobile.mosby.rates;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

/**
 * @author QuickNick
 */
public interface IRatesPresenter extends MvpPresenter<IRatesView> {

    void loadRates(boolean pullToRefresh);
}
