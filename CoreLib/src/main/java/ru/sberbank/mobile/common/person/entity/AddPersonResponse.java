package ru.sberbank.mobile.common.person.entity;

import com.google.common.base.Objects;

import ru.sberbank.mobile.core.bean.operation.ServerEntity;
import ru.sberbank.mobile.core.network.ConnectorStatus;
import ru.sberbank.mobile.core.observ.ConditionalStatusedEntityWatcher;

/**
 * Сущность, необходимая для демонстрации сценария с {@link ConditionalStatusedEntityWatcher}.
 *
 * Менеджер персон при добавлении персоны сделает вид, что операция долгая, после чего вернёт результат со статусом.
 *
 * @author QuickNick.
 */
public class AddPersonResponse extends ServerEntity {

    public long id;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AddPersonResponse)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AddPersonResponse that = (AddPersonResponse) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), id);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("super", super.toString())
                .add("id", id)
                .toString();
    }

    @Override
    public boolean isSuccess() {
        return getConnectorStatus() == ConnectorStatus.SUCCESS;
    }
}
