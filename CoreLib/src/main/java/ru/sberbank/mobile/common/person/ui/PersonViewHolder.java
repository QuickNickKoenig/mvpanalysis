package ru.sberbank.mobile.common.person.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.R;

/**
 * @author QuickNick
 */
public class PersonViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTextView;
    private final OnPersonClickListener mOnPersonClickListener;
    private Person mPerson;

    public PersonViewHolder(View itemView, OnPersonClickListener onPersonClickListener) {
        super(itemView);
        mTextView = (TextView) itemView;
        mOnPersonClickListener = onPersonClickListener;
        itemView.setOnClickListener(new OnItemViewClickListener());
    }

    public static PersonViewHolder newHolder(ViewGroup parent, OnPersonClickListener onPersonClickListener) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.simple_text_item, parent, false);
        return new PersonViewHolder(itemView, onPersonClickListener);
    }

    public void bindView(Person person) {
        mPerson = person;
        mTextView.setText(person.name);
    }

    public interface OnPersonClickListener {

        void onPersonClick(PersonViewHolder sender, Person person);
    }

    private class OnItemViewClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (mPerson != null) {
                mOnPersonClickListener.onPersonClick(PersonViewHolder.this, mPerson);
            }
        }
    }
}
