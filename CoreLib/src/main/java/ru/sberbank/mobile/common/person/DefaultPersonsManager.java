package ru.sberbank.mobile.common.person;

import android.os.SystemClock;

import java.util.List;

import ru.sberbank.mobile.common.person.db.IPersonsDao;
import ru.sberbank.mobile.common.person.entity.AddPersonResponse;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.IAsyncProcessor;
import ru.sberbank.mobile.core.async.NoExceptionCallable;
import ru.sberbank.mobile.core.async.PendingResult;

/**
 * @author QuickNick.
 */
public class DefaultPersonsManager implements IPersonsManager {

    private static final long SLEEP_TIME_IN_MILLIS = 6000;

    private final IAsyncProcessor mAsyncProcessor;
    private final IPersonsDao mPersonsDao;

    public DefaultPersonsManager(IAsyncProcessor asyncProcessor, IPersonsDao personsDao) {
        mAsyncProcessor = asyncProcessor;
        mPersonsDao = personsDao;
    }

    @Override
    public PendingResult<List<Person>> getPersons() {
        PendingResult<List<Person>> result = mAsyncProcessor.submit(
                PersonUris.PERSONS_URI,
                new NoExceptionCallable<List<Person>>() {
                    @Override
                    public List<Person> call() {
                        return mPersonsDao.getPersons();
                    }
                },
                false
        );
        return result;
    }

    @Override
    public PendingResult<AddPersonResponse> addPerson(final Person person) {
        PendingResult<AddPersonResponse> result = mAsyncProcessor.submit(
                PersonUris.getAddPersonUri(person),
                new NoExceptionCallable<AddPersonResponse>() {
                    @Override
                    public AddPersonResponse call() {
                        return addPersonInternal(person);
                    }
                },
                false
        );
        return result;
    }

    @Override
    public PendingResult<Integer> updatePerson(final Person person) {
        PendingResult<Integer> result = mAsyncProcessor.submit(
                PersonUris.getUpdatePersonUri(person),
                new NoExceptionCallable<Integer>() {
                    @Override
                    public Integer call() {
                        return updatePersonInternal(person);
                    }
                },
                false
        );
        return result;
    }

    @Override
    public PendingResult<Integer> deletePerson(final Person person) {
        PendingResult<Integer> result = mAsyncProcessor.submit(
                PersonUris.getDeletePersonUri(person),
                new NoExceptionCallable<Integer>() {
                    @Override
                    public Integer call() {
                        return deletePersonInternal(person);
                    }
                },
                false
        );
        return result;
    }

    private AddPersonResponse addPersonInternal(Person person) {
        SystemClock.sleep(SLEEP_TIME_IN_MILLIS);
        long id = mPersonsDao.addPerson(person);
        AddPersonResponse response = new AddPersonResponse();
        response.id = id;
        mAsyncProcessor.markTaskAsDirty(PersonUris.PERSONS_URI);
        return response;
    }

    private Integer updatePersonInternal(Person person) {
        Integer result = mPersonsDao.updatePerson(person);
        mAsyncProcessor.markTaskAsDirty(PersonUris.PERSONS_URI);
        return result;
    }

    private Integer deletePersonInternal(Person person) {
        Integer result = mPersonsDao.deletePerson(person);
        mAsyncProcessor.markTaskAsDirty(PersonUris.PERSONS_URI);
        return result;
    }
}
