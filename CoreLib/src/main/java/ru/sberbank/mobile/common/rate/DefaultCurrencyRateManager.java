package ru.sberbank.mobile.common.rate;

import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.common.rate.net.ICurrencyRateApiMapper;
import ru.sberbank.mobile.core.async.IAsyncProcessor;
import ru.sberbank.mobile.core.async.NoExceptionCallable;
import ru.sberbank.mobile.core.async.PendingResult;

/**
 * @author QuickNick.
 */
public class DefaultCurrencyRateManager implements ICurrencyRateManager {

    private final IAsyncProcessor mAsyncProcessor;
    private final ICurrencyRateApiMapper mApiMapper;

    public DefaultCurrencyRateManager(IAsyncProcessor asyncProcessor, ICurrencyRateApiMapper apiMapper) {
        mAsyncProcessor = asyncProcessor;
        mApiMapper = apiMapper;
    }

    @Override
    public PendingResult<RatesBundle> getRatesBundle() {
        PendingResult<RatesBundle> result = mAsyncProcessor.submit(
                RateUris.RATES_BUNDLE_URI, new NoExceptionCallable<RatesBundle>() {
                    @Override
                    public RatesBundle call() {
                        return mApiMapper.getRatesBundle();
                    }
                }, false
        );
        return result;
    }
}
