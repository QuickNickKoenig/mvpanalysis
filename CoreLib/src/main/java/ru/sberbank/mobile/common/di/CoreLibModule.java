package ru.sberbank.mobile.common.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.sberbank.mobile.common.person.DefaultPersonsManager;
import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.db.IPersonsDao;
import ru.sberbank.mobile.common.person.db.SQLitePersonsDao;
import ru.sberbank.mobile.common.person.db.SQLitePersonsHelper;
import ru.sberbank.mobile.common.rate.DefaultCurrencyRateManager;
import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.net.DefaultCurrencyRateApiMapper;
import ru.sberbank.mobile.common.rate.net.ICurrencyRateApiMapper;
import ru.sberbank.mobile.core.async.IAsyncProcessor;
import ru.sberbank.mobile.core.network.IHttpConnector;
import ru.sberbank.mobile.core.observ.DefaultPendingResultRetriever;
import ru.sberbank.mobile.core.observ.IPendingResultRetriever;
import ru.sberbank.mobile.core.parser.ExtendedParser;
import ru.sberbank.mobile.core.parser.SimpleXMLParser;

@Module
public class CoreLibModule {

    private final Context mApplicationContext;

    public CoreLibModule(Context applicationContext) {
        mApplicationContext = applicationContext;
    }

    @Singleton
    @Provides
    public IPendingResultRetriever providePendingResultRetriever(IAsyncProcessor asyncProcessor) {
        return new DefaultPendingResultRetriever(asyncProcessor);
    }

    @Singleton
    @Provides
    public IPersonsManager providePersonsManager(IAsyncProcessor asyncProcessor) {
        IPersonsDao contactsDao = new SQLitePersonsDao(new SQLitePersonsHelper(mApplicationContext));
        IPersonsManager manager = new DefaultPersonsManager(asyncProcessor, contactsDao);
        return manager;
    }

    @Singleton
    @Provides
    public ICurrencyRateManager provideCurrencyRateManager(IAsyncProcessor asyncProcessor,
                                                           IHttpConnector httpConnector) {
        ExtendedParser parser = new ExtendedParser(new SimpleXMLParser());
        ICurrencyRateApiMapper apiMapper = new DefaultCurrencyRateApiMapper(httpConnector, parser);
        ICurrencyRateManager manager = new DefaultCurrencyRateManager(asyncProcessor, apiMapper);
        return manager;
    }
}
