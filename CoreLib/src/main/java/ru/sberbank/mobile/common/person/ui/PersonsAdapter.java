package ru.sberbank.mobile.common.person.ui;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.utils.CollectionUtils;

/**
 * @author QuickNick
 */
public class PersonsAdapter extends RecyclerView.Adapter<PersonViewHolder> {

    private final PersonViewHolder.OnPersonClickListener mOnPersonClickListener;
    private final List<Person> mPersons;

    public PersonsAdapter(PersonViewHolder.OnPersonClickListener onPersonClickListener) {
        mOnPersonClickListener = onPersonClickListener;
        mPersons = new ArrayList<>();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return PersonViewHolder.newHolder(parent, mOnPersonClickListener);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.bindView(mPersons.get(position));
    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }

    public void setPersons(List<Person> persons) {
        CollectionUtils.clearAndFill(persons, mPersons);
        notifyDataSetChanged();
    }
}
