package ru.sberbank.mobile.core.observ;

import android.content.Context;

import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.bean.operation.StatusedEntity;
import ru.sberbank.mobile.core.check.IResultChecker;

/**
 * Created by krygin on 15.12.16.
 */

public class ShowSuccessMessagesContentWatcher<T extends StatusedEntity>
        extends AbstractStatusedEntityWatcher<T> {

    public ShowSuccessMessagesContentWatcher(Context context, IResultChecker<? super T> resultChecker, PendingResult pendingResult) {
        super(context, resultChecker, pendingResult);
    }

    @Override
    public boolean isConditionalContentWatcher() {
        return false;
    }

    @Override
    protected PendingResult<T> obtain(boolean force) {
        return getPendingResult();
    }

    @Override
    protected boolean shouldShowSuccessMessages() {
        return true;
    }
}
