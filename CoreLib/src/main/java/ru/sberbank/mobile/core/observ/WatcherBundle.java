package ru.sberbank.mobile.core.observ;

import android.net.Uri;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ru.sberbank.mobile.core.function.Predicate;
import ru.sberbank.mobile.core.utils.CollectionUtils;

/**
 * Created by krygin on 07.11.16.
 */

public class WatcherBundle implements IWatcher {

    private final IContentWatcherCreator mContentWatcherCreator;

    public WatcherBundle() {
        mContentWatcherCreator = null;
    }

    public WatcherBundle(IContentWatcherCreator contentWatcherCreator) {
        mContentWatcherCreator = contentWatcherCreator;
    }

    private static final String KEY_WATCHER = "KEY_WATCHERS";
    private List<IContentWatcher> mContentWatcherList = new ArrayList<>();
    private boolean mResumed;

    @Override
    public void onResume() {
        mResumed = true;
        List<IContentWatcher> watchers = new ArrayList<>(mContentWatcherList);
        for (IContentWatcher watcher: watchers) {
            watcher.onResume();
        }
    }

    @Override
    public void onPause() {
        mResumed = false;
        List<IContentWatcher> watchers = new ArrayList<>(mContentWatcherList);
        for (IContentWatcher watcher: watchers) {
            watcher.onPause();
        }
    }

    public void add(IContentWatcher watcher) {
        mContentWatcherList.add(watcher);
        if (mResumed) {
            watcher.onResume();
        }
    }

    public void remove(IContentWatcher watcher) {
        mContentWatcherList.remove(watcher);
        watcher.onPause();
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(KEY_WATCHER, retrieveWatcherUris());
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        List<Uri> uris = savedInstanceState.getParcelableArrayList(KEY_WATCHER);
        if (uris != null && mContentWatcherCreator != null) {
            for (Uri uri : uris) {
                mContentWatcherList.add(mContentWatcherCreator.create(uri));
            }
        }
    }

    private ArrayList<Uri> retrieveWatcherUris() {
        ArrayList<Uri> uris = new ArrayList<>();
        for (IContentWatcher watcher: getActionContentWatcher()) {
            uris.add(watcher.getPendingResultUri());
        }
        return uris;
    }

    private List<IContentWatcher> getActionContentWatcher() {
        return CollectionUtils.filter(mContentWatcherList, new Predicate<IContentWatcher>() {
            @Override
            public boolean apply(IContentWatcher arg) {
                return arg.isConditionalContentWatcher();
            }
        });
    }


}
