package ru.sberbank.mobile.core.utils;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Утилиты для работы с Uri. Своего рода {@code ContentUris} из Android, но
 * только заточенный под наши урлы и не такой убогий.
 *
 * <p>Включает в себя вспомогательные константы для использования без жёстко заданных ограничений.
 * Типичные имена действий и т.п.</p>
 *
 * @author sokol @ 02.08.16
 */
public final class UriUtils {

    /**
     * <p>Параметр Uri для типовых действий с контентом. Значение: "{@value}".</p>
     * <p>Может использоваться в extras, т.е.</p>
     *
     * <pre>
     *     some_uri?{@value}=something</pre>
     *
     * <p>В качестве значений extra удобно использовать {@link CommonActions}, например:</p>
     * <pre class="prettyprint">
     *     uri = uriManager
     *              .feature("something")
     *              .path("something/else")
     *              .extra(UriUtils.ACTION, UriUtils.CommonActions.delete.name());</pre>
     */
    public static final String ACTION = "action";

    /**
     * Наиболее частые действия для {@link #ACTION}. Естественно, никаких ограничений здесь нет,
     * просто удобно использовать некие определённые заранее значения.
     */
    public enum CommonActions {
        /**
         * Типичное действие: «включить»
         */
        enable,
        /**
         * Типичное действие: «создать»
         */
        create,
        /**
         * Типичное действие: «обновить»
         */
        update,
        /**
         * Типичное действие: «удалить»
         */
        delete,
        /**
         * Типичное действие: выписка по операциям
         */
        operations,
        /**
         * Типичное действие: скрыть
         */
        hide,
        /**
         * Типичное действие: сделать видимым
         */
        show,
        /**
         * Типичное действие: инициализировать
         */
        init,
        /**
         * Типичное действие: сохранить
         */
        save,
        /**
         * Типичное действие: отменить
         */
        cancel,

        /**
         * Типичное действие: подтвердить
         */
        confirm,

        /**
         * Типичное действие: редактирование
         */
        edit,

        /**
         * Типичное действие: дальше
         */
        next,

        /**
         * Типичное действие: просмотреть список чего либо (список автоплатежей)
         */
        list
    }


    private UriUtils() {
        // Блокируем создание объекта
    }

    /**
     * Полный аналог {@code ContentUris.withAppendedId()} из Android. Добавляет в конец Uri
     * некий числовой id. При этом сохраняются все остальные части URI. Например, для
     * id == 100500:
     *
     * <table class="pretty">
     *     <tbody>
     *         <tr>
     *             <th>Исходный URI</th>
     *             <th>Результат</th>
     *         </tr>
     *
     *         <tr>
     *             <td>android-app://ru.sberbankmobile/example</td>
     *             <td>android-app://ru.sberbankmobile/example/100500</td>
     *         </tr>
     *
     *         <tr>
     *             <td>android-app://ru.sberbankmobile/example?foo=bar</td>
     *             <td>android-app://ru.sberbankmobile/example/100500?foo=bar</td>
     *         </tr>
     *
     *         <tr>
     *             <td>mailto:test@example.com</td>
     *             <td><span style="color:red;">IllegalArgumentException</span></td>
     *         </tr>
     *         <tr>
     *             <td>/foo/bar</td>
     *             <td><span style="color:red;">IllegalArgumentException</span></td>
     *         </tr>
     *     </tbody>
     * </table>
     *
     *
     * <p>Как можно заметить из последнего примера, метод требует в том числе полноты переданного
     * Uri (относительные Uri не поддерживаются).</p>
     *
     * @param source Исходный Uri
     * @param id Числовой id
     * @return Созданный Uri
     * @throws IllegalArgumentException Если переданный Uri некорректен
     */
    public static Uri withAppendedId(Uri source, long id) {
        if (!source.isHierarchical() || source.isRelative()) {
            throw new IllegalArgumentException("Only hierarchical absolute URIs is supported");
        }

        String partToAppend = Long.toString(id);

        Uri.Builder builder = source.buildUpon();
        builder.appendEncodedPath(partToAppend);
        return builder.build();
    }

    /**
     * Заменяет в переданной строке некоторые символы для возможности её использования в path
     *
     * @param rawPath Сырая строка
     * @return Строка, <strong>вероятно</strong> пригодная к использованию в качестве path.
     */
    public static String escapePath(String rawPath) {
        String result = rawPath.trim();
        result = result.replaceAll(":", "/");
        return result;
    }

    /**
     * Проверяет, является ли один Uri иерархическим потомком другого. При этом extra (часть после
     * «?» с аргументами) игнорируется.
     *
     * <p>Например, у нас есть Uri</p>
     *
     * <pre>
     *     android-app://ru.sberbankmobile/foo</pre>
     *
     * <p>Потомками для него будут:</p>
     *
     * <pre>
     *     android-app://ru.sberbankmobile/foo/bar
     *     android-app://ru.sberbankmobile/foo/bar/baz
     *     android-app://ru.sberbankmobile/foo/bar?cat=1&dog=2</pre>
     *
     * <p>Но при этом НЕ будут являться потомками:</p>
     *
     * <pre>
     *     android-app://ru.sberbankmobile/foo?cat=1
     *     android-app://ru.sberbankmobile/foobar</pre>
     *
     * <p>В первом примере добавилась только extra, во втором Uri вовсе другой, хотя его строковое
     * представление начинается одинаково. Верно и наоборот, что для Uri</p>
     *
     * <pre>
     *     android-app://ru.sberbankmobile/foo?cat=1</pre>
     *
     * <p>(т.е. с extra), являться потомками <strong>будут</strong> все те же Uri, что для первого
     * примера выше.</p>
     *
     * <p>Следите за порядком аргументов. В общем случае действует правило:</p>
     *
     * <pre class="prettyprint">
     *     if (UriUtils.isNestedUri(a, b) == true) {
     *         UriUtils.isNestedUri(b, a);  // == false
     *     }</pre>
     *
     * @param parent Родительский Uri
     * @param check Потенциально вложенный в него Uri
     * @return {@code true} если иерархия найдена, {@code false} иначе.
     */
    public static boolean isNestedUri(@NonNull Uri parent, @NonNull Uri check) {
        // Быстрых проверок не может быть, потому что extra мы игнорируем, а их длина
        // абсолютно произвольна (сравнение длин строк не катит)

        StringBuilder parentBuilder = new StringBuilder(parent.toString());
        StringBuilder checkBuilder = new StringBuilder(check.toString());

        // Нормализуем представление двух Uri
        int parentPos = parentBuilder.indexOf("?");
        int checkPos = checkBuilder.indexOf("?");

        if (parentPos > 0) {
            parentBuilder.delete(parentPos, parentBuilder.length());
        }

        if (checkPos > 0) {
            checkBuilder.delete(checkPos, checkBuilder.length());
        }

        // Для нормализованной формы добавляем в хвост «/»
        if (parentBuilder.charAt(parentBuilder.length() - 1) != '/') {
            parentBuilder.append("/");
        }

        String parentAsString = parentBuilder.toString();
        String checkAsString = checkBuilder.toString();

        if (checkAsString.equals(parentAsString)) {
            return false;
        }

        return checkAsString.startsWith(parentAsString);
    }

    /**
     * Проверяет, является ли один Uri иерархическим потомком другого. При этом extra (часть после
     * «?» с аргументами) игнорируется. В случае эквивалентности Uri  - возвращает true
     */
    public static boolean isEqualOrNestedUri(@NonNull Uri parent, @NonNull Uri check) {
        String normalizedParentUri = cutQueryParamsAndNormalize(parent.toString());
        String normalizedCheckUri = cutQueryParamsAndNormalize(check.toString());
        return isEqualOrNestedString(normalizedParentUri, normalizedCheckUri);
    }

    /**
     * Проверяет, эквивалентна ли одна строка другой или является ее иерархическим потомком другой
     */
    static boolean isEqualOrNestedString(String parent, String child) {
        return child.startsWith(parent);
    }

    /**
     * Возвращает список фрагментов пути, на которые child uri длиннее parent uri.
     * Если child uri не является иерархическим потомком parent uri - метод вернет пустой список.
     */
    public static List<String> getRestPathSegments(@NonNull Uri parent, @NonNull Uri child) {
        List<String> pathSegments = new ArrayList<>();

        String normalizedParentUri = cutQueryParamsAndNormalize(parent.toString());
        String normalizedChildUri = cutQueryParamsAndNormalize(child.toString());

        if (isEqualOrNestedString(normalizedParentUri, normalizedChildUri)) {
            String restPath = normalizedChildUri.substring(normalizedParentUri.length());
            pathSegments.addAll(Arrays.asList(restPath.split("/")));
        }

        return pathSegments;
    }

    /**
     * Удаляет все query - параметры (extra) и принудительно завершает url символом '/'
     */
    static String cutQueryParamsAndNormalize(String uri) {
        StringBuilder parentBuilder = new StringBuilder(uri.toString());

        int parentPos = parentBuilder.indexOf("?");

        // Удаляем параметры
        if (parentPos > 0) {
            parentBuilder.delete(parentPos, parentBuilder.length());
        }

        // Для нормализованной формы добавляем в хвост «/»
        if (parentBuilder.charAt(parentBuilder.length() - 1) != '/') {
            parentBuilder.append("/");
        }

        return parentBuilder.toString();
    }

}