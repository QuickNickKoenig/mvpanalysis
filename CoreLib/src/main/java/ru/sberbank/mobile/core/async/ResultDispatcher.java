package ru.sberbank.mobile.core.async;

/**
 * @author QuickNick
 */
interface ResultDispatcher<T> {

    void onBecomeDirty(PendingResult<T> sender);

    void onLoaded(PendingResult<T> sender);

    void detachMe(PendingResult<T> sender);
}
