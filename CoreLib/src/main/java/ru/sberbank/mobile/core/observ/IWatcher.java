package ru.sberbank.mobile.core.observ;


/**
 * Created by krygin on 07.11.16.
 */

public interface IWatcher {
    void onResume();
    void onPause();
}
