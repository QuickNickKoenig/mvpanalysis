package ru.sberbank.mobile.core.async;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Процессор только для тестов! Не используйте его на бою!
 *
 * @author QuickNick
 */
public class StubAsyncProcessor implements IAsyncProcessor {

    private final Context mContext;
    private final Map<Uri, StubPendingResult<?>> mPendingResultMap;

    public StubAsyncProcessor(Context context) {
        mContext = context;
        mPendingResultMap = new HashMap<>();
    }

    @Override
    public <T> PendingResult<T> submit(@NonNull Uri uri, @NonNull NoExceptionCallable<T> task, boolean forceReplace) {
        StubPendingResult<T> pending = (StubPendingResult<T>) mPendingResultMap.get(uri);
        if (forceReplace || (pending == null)) {
            pending = new StubPendingResult<>(uri, task);
            mPendingResultMap.put(uri, pending);
            mContext.getContentResolver().notifyChange(uri, null);
        }
        return pending;
    }

    @Override
    public <T> PendingResult<T> findResult(@NonNull Uri uri) {
        return (PendingResult<T>) mPendingResultMap.get(uri);
    }

    @Override
    public List<Uri> getTasksUris(@NonNull TaskFilter filter) {
        Map<Uri, StubPendingResult<?>> syncMap = Collections.synchronizedMap(mPendingResultMap);
        List<Uri> result = new ArrayList<>();

        for (Uri uri : syncMap.keySet()) {
            StubPendingResult<?> value = syncMap.get(uri);

            // Мало ли что, в реализации synchronizedMap никак не гарантировано сохранение ключей
            // и значений одновременно
            if (value == null) {
                continue;
            }

            if (filter.filter(uri, value)) {
                result.add(uri);
            }
        }

        return Collections.unmodifiableList(result);
    }

    @Override
    public void clearTasks(@NonNull TaskFilter filter) {
        List<Uri> uris = getTasksUris(filter);
        for (Uri uri : uris) {
            clearTask(uri);
        }
    }

    @Override
    public boolean clearTask(@Nullable Uri uri) {
        boolean cleared = false;
        if (uri != null) {
            StubPendingResult<?> task = mPendingResultMap.get(uri);
            if ((task != null) && !task.isLoading()) {
                mPendingResultMap.remove(uri);
                cleared = true;
            }
        }
        return cleared;
    }

    @Override
    public void markTaskAsDirty(@NonNull Uri uri) {
        PendingResult<?> task = mPendingResultMap.get(uri);
        if (task != null) {
            task.markAsDirty();
        }
    }

    @Override
    public void notifyChange(@NonNull Uri uri) {
        mContext.getContentResolver().notifyChange(uri, null);
    }
}
