package ru.sberbank.mobile.core.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.sberbank.mobile.core.async.IAsyncProcessor;
import ru.sberbank.mobile.core.async.MultiThreadAsyncProcessor;
import ru.sberbank.mobile.core.network.DefaultHttpConnector;
import ru.sberbank.mobile.core.network.IHttpConnector;

/**
 * @author QuickNick
 */
@Module
public class BaseModelsModule {

    private final Application mApplication;

    public BaseModelsModule(Application application) {
        mApplication = application;
    }

    @Singleton
    @Provides
    public IAsyncProcessor provideAsyncProcessor() {
        return new MultiThreadAsyncProcessor(mApplication);
    }

    @Singleton
    @Provides
    public IHttpConnector provideHttpConnector() {
        return new DefaultHttpConnector();
    }

    @Singleton
    @Provides
    public Application provideApplication() {
        return mApplication;
    }

    @Singleton
    @Provides
    public Context provideApplicationContext() {
        return mApplication;
    }
}
