package ru.sberbank.mobile.core.async;

import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * @author QuickNick
 */
public class StubPendingResult<T> implements PendingResult<T> {

    private final Uri mUri;
    private final T mResult;

    private boolean mLoading = false;

    public StubPendingResult(Uri uri, NoExceptionCallable<T> callable) {
        mUri = uri;
        mResult = callable.call();
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void markAsDirty() {
    }

    @Override
    public boolean isLoading() {
        return mLoading;
    }

    @Override
    public boolean isReady() {
        return !isLoading() && !isDirty();
    }

    @Nullable
    @Override
    public T getResult() {
        return mResult;
    }

    @Nullable
    @Override
    public T awaitAndGetResult() {
        return mResult;
    }

    @Override
    public void detach() {

    }

    @Override
    public Uri getUri() {
        return mUri;
    }

    public void overrideIsLoading(boolean loading) {
        mLoading = loading;
    }
}
