package ru.sberbank.mobile.core.observ;

import android.net.Uri;

import ru.sberbank.mobile.core.async.IAsyncProcessor;
import ru.sberbank.mobile.core.async.PendingResult;

/**
 * @author QuickNick.
 */
public class DefaultPendingResultRetriever implements IPendingResultRetriever {

    private final IAsyncProcessor mAsyncProcessor;

    public DefaultPendingResultRetriever(IAsyncProcessor asyncProcessor) {
        mAsyncProcessor = asyncProcessor;
    }

    @Override
    public <T> PendingResult<T> retrieve(Uri uri) {
        return mAsyncProcessor.findResult(uri);
    }
}
