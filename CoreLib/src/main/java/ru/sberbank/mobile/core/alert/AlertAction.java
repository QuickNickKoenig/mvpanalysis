package ru.sberbank.mobile.core.alert;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.lang.reflect.Modifier;

/**
 * @author QuickNick
 */
public abstract class AlertAction implements Serializable {

    public AlertAction() {
        boolean outerClazz = (getClass().getEnclosingClass() == null);
        boolean staticClazz = !outerClazz && Modifier.isStatic(getClass().getModifiers());
        boolean valid = outerClazz || staticClazz;
        if (!valid) {
            throw new RuntimeException("Действие алерта для предотвращения утечки памяти должно быть " +
                    "или самостоятельным классом, или статическим внутренним");
        }
    }

    public abstract void onEvent(@NonNull AlertDialogFragment sender);
}
