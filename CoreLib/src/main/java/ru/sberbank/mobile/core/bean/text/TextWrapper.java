package ru.sberbank.mobile.core.bean.text;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Класс, поддерживающий построение ресурсных строк в отсутствие Context-а.
 * Сценарий использования: в модели строится TextWrapper, ему передается некоторый textResId или статичный text.
 * В слое Presenter-a ему подается контекст, с помощью которого класс строит строку.
 */
public class TextWrapper implements Serializable, Parcelable {

    public static final Parcelable.Creator<TextWrapper> CREATOR = new TextWrapperParcelableCreator();

    @StringRes
    private int mTextResId;
    private String mText;

    private TextWrapper() {
    }

    public TextWrapper(@StringRes int textResId) {
        this.mTextResId = textResId;
    }

    public TextWrapper(String text) {
        this.mText = text;
    }

    public TextWrapper(Parcel source) {
        mTextResId = source.readInt();
        mText = source.readString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TextWrapper)) {
            return false;
        }
        TextWrapper that = (TextWrapper) o;
        return mTextResId == that.mTextResId &&
                Objects.equal(mText, that.mText);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mTextResId, mText);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("mTextResId", mTextResId)
                .add("mText", mText)
                .toString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mTextResId);
        dest.writeString(mText);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @NonNull
    public CharSequence getText(Context context) {
        CharSequence result = (mText == null) ? context.getText(mTextResId) : mText;
        return result;
    }

    private static class TextWrapperParcelableCreator implements Parcelable.Creator<TextWrapper> {

        @Override
        public TextWrapper createFromParcel(Parcel source) {
            TextWrapper wrapper = new TextWrapper(source);
            return wrapper;
        }

        @Override
        public TextWrapper[] newArray(int size) {
            return new TextWrapper[size];
        }
    }
}
