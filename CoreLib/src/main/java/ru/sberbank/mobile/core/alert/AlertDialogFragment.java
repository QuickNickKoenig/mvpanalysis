package ru.sberbank.mobile.core.alert;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * @author QuickNick
 */
public class AlertDialogFragment extends DialogFragment {

    public static final String TAG = AlertDialogFragment.class.getSimpleName();
    private static final String KEY_ALERT_DESCRIPTION = "alert_description";

    private AlertDescription mAlertDescription;

    public static Bundle newArgs(@NonNull AlertDescription description) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_ALERT_DESCRIPTION, description);
        return args;
    }

    public static AlertDialogFragment newInstance(@NonNull AlertDescription description) {
        AlertDialogFragment fragment = new AlertDialogFragment();
        fragment.setArguments(newArgs(description));
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mAlertDescription = getArguments().getParcelable(KEY_ALERT_DESCRIPTION);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (mAlertDescription.getTitle() != null) {
            builder.setTitle(mAlertDescription.getTitle().getText(context));
        }
        if (mAlertDescription.getMessage() != null) {
            builder.setMessage(mAlertDescription.getMessage().getText(context));
        }
        AlertDescription.ButtonAction positiveAction = mAlertDescription.getPositiveButton();
        if (positiveAction != null) {
            builder.setPositiveButton(positiveAction.getLabel().getText(context),
                    makeListener(positiveAction));
        }
        AlertDescription.ButtonAction neutralAction = mAlertDescription.getNeutralButton();
        if (neutralAction != null) {
            builder.setNeutralButton(neutralAction.getLabel().getText(context),
                    makeListener(neutralAction));
        }
        AlertDescription.ButtonAction negativeAction = mAlertDescription.getNegativeButton();
        if (negativeAction != null) {
            builder.setNegativeButton(negativeAction.getLabel().getText(context),
                    makeListener(negativeAction));
        }
        return builder.create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        AlertAction action = mAlertDescription.getOnDismissAction();
        if (action != null) {
            action.onEvent(this);
        }
    }

    private DialogInterface.OnClickListener makeListener(final AlertDescription.ButtonAction buttonAction) {
        DialogInterface.OnClickListener listener = null;
        if (buttonAction.getAction() != null) {
            listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    buttonAction.getAction().onEvent(AlertDialogFragment.this);
                }
            };
        }
        return listener;
    }
}
