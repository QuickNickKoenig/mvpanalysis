package ru.sberbank.mobile.core.di;

/**
 * @author Andrey Mamonov.
 */

public interface IHasComponent<T> {

    T getComponent();
}
