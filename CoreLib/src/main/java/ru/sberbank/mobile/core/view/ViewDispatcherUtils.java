package ru.sberbank.mobile.core.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

/**
 * @author QuickNick.
 */

public class ViewDispatcherUtils {

    public static void setContentView(AppCompatActivity activity, Bundle savedInstanceState, IViewDispatcher dispatcher) {
        View view = dispatcher.createView(LayoutInflater.from(activity), null, savedInstanceState);
        activity.setContentView(view);
    }
}
