package ru.sberbank.mobile.core.alert.actions;

import android.app.Activity;
import android.support.annotation.NonNull;

import ru.sberbank.mobile.core.alert.AlertAction;
import ru.sberbank.mobile.core.alert.AlertDialogFragment;

/**
 * {@linkplain AlertAction Alert action}, завершающий активити. Пригодится в тех ситуациях, когда мы
 * должны вывести диалог и завершить деятельность на экране после его прочтения. Например, если мы
 * хотели просмотреть продукт или операцию, а такой объект уже удалён.
 *
 * <p>Не имеет никаких побочных эффектов и проверок кроме вызова {@code getActivity().finish()}.
 * Для ситуаций «цепочка диалогов и финиш на последнем из них» рекомендуется продумать отдельную
 * логику.</p>
 *
 * @author Дмитрий Соколов
 */
public class FinishActivityAlertAction extends AlertAction {

    @Override
    public void onEvent(@NonNull AlertDialogFragment sender) {
        if (sender.getActivity() != null) {
            sender.getActivity().finish();
        }
    }
}
