package ru.sberbank.mobile.core.async;

import android.support.annotation.Nullable;

import com.google.common.base.Objects;

/**
 * Внутренний класс, используемый для хранения контента в реализациях {@link PendingResult}. Может
 * быть полезен для тех случаев, когда контент оказывается {@code null} в результате загрузки. В
 * итоге нельзя проверять готовность просто через:
 *
 * <div>
 * <pre class="prettyprint">
 *     if (mContent == null) {
 *         // Может, загружено… может и нет
 *     }
 * </pre>
 * </div>
 *
 * <p>А благодаря такой обёртке проверка становится валидной: сам контент придётся получать через
 * вызов {@link #get()}. Для пользователя core это означает возможность проверки через
 * {@link PendingResult#isReady()} факта готовности данных.</p>
 *
 *
 * <p>Это immutable-объект, потому метод set() не предусмотрен и не должен добавляться.</p>
 */
final class Content<Type> {

    private final Type mContent;

    // Из задумок: добавить сюда timestamp

    Content(@Nullable Type content) {
        mContent = content;
    }

    Type get() {
        return mContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Content)) {
            return false;
        }
        Content<?> content = (Content<?>) o;
        return Objects.equal(mContent, content.mContent);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mContent);
    }
}
