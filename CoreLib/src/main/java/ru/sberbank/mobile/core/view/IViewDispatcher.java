package ru.sberbank.mobile.core.view;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.sberbank.mobile.core.alert.AlertDescription;

/**
 * @author QuickNick
 */
public interface IViewDispatcher {

    @Nullable
    View createView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState);

    void destroyView();

    /**
     * Метод, который должен вызываться когда фрагменты готовы к работе (иначе говоря, когда
     * манипуляции с ними не будут приводить к IllegalStateException). Без его вызова в ряде ситуаций
     * не будут работать отложенные диалоги и т.п.
     */
    void onFragmentsReady();

    /**
     * Отображает пользователю указанный диалог, если имеется такая возможность.
     * @param description Описание диалога
     * @return <code>true</code> если диалог показан, <code>false</code> иначе.
     */
    boolean showDialog(@NonNull AlertDescription description);

    @NonNull
    Resources getResources();
}
