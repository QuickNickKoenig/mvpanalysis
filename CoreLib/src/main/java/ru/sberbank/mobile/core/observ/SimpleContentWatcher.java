package ru.sberbank.mobile.core.observ;

import android.content.Context;
import android.net.Uri;

import ru.sberbank.mobile.core.async.PendingResult;

/**
 * @author QuickNick
 */

public abstract class SimpleContentWatcher<T> implements IContentWatcher {

    private final Context mContext;
    private PendingResult<T> mPendingResult;
    private final ISbolObserverListener mListener = new ISbolObserverListener() {

        @Override
        public void onContentChanged() {
            checkData();
        }
    };
    private final SbolContentObserver mContentObserver;

    public SimpleContentWatcher(Context context) {
        mContext = context;
        mContentObserver = new SbolContentObserver(mListener);
    }

    @Override
    public void onResume() {
        mPendingResult = obtain(false);
        mContext.getContentResolver().registerContentObserver(mPendingResult.getUri(), false, mContentObserver);
        checkData();
    }

    @Override
    public void onPause() {
        mContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    @Override
    public final Uri getPendingResultUri() {
        return mPendingResult != null ? mPendingResult.getUri() : Uri.EMPTY;
    }

    @Override
    public boolean isConditionalContentWatcher() {
        return false;
    }

    @Override
    public final void refresh() {
        if (!isConditionalContentWatcher()) {
            mPendingResult.markAsDirty();
        }
    }

    @Override
    public final void forceReload() {
        if (!isConditionalContentWatcher()) {
            mPendingResult = obtain(true);
        }
    }

    @Override
    public void detachResult() {
        mPendingResult.detach();
    }

    protected abstract PendingResult<T> obtain(boolean force);
    protected abstract void onLoaded(T result);

    protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
    }

    protected Context getContext() {
        return mContext;
    }

    protected PendingResult<T> getPendingResult() {
        return mPendingResult;
    }

    protected void setPendingResult(PendingResult<T> pendingResult) {
        mPendingResult = pendingResult;
    }

    private void checkData() {
        if (mPendingResult.isLoading()) {
            onLoadStateChanged(this, true);
        } else {
            onLoadStateChanged(this, false);
            final T result = mPendingResult.getResult();
            onLoaded(result);
        }
    }
}
