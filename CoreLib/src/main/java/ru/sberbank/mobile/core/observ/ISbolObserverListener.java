package ru.sberbank.mobile.core.observ;

/**
 * Created by SBT-Gontarenko-AA
 */
public interface ISbolObserverListener {
    void onContentChanged();
}
