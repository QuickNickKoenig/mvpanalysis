package ru.sberbank.mobile.core.view;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import ru.sberbank.mobile.core.alert.AlertDescription;
import ru.sberbank.mobile.core.alert.AlertDialogFragment;

/**
 * @author QuickNick
 */
public class BaseViewDispatcher implements IViewDispatcher {

    protected final Context mContext;
    protected final FragmentManager mFragmentManager;
    protected View mView;
    private final Queue<AlertDescription> mPendingAlerts = new ArrayDeque<>();

    public BaseViewDispatcher(Context context, FragmentManager fragmentManager) {
        this.mContext = context;
        this.mFragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public final View createView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = onCreateView(inflater, container, savedInstanceState);
        return mView;
    }

    @Override
    public void destroyView() {
        onDestroyView();
        mView = null;
    }

    @Override
    public void onFragmentsReady() {
        AlertDescription firstAlert = mPendingAlerts.poll();
        if (firstAlert != null) {
            showDialog(firstAlert);
        }
    }

    @Override
    public boolean showDialog(@NonNull AlertDescription description) {
        try {
            AlertDialogFragment fragment = AlertDialogFragment.newInstance(description);
            fragment.show(mFragmentManager, AlertDialogFragment.TAG);
            return true;

        } catch (IllegalStateException fragmentsNotReady) {
            if (!mPendingAlerts.contains(description)) {
                mPendingAlerts.offer(description);
            }

            // Диалог так и не был показан
            return false;
        }
    }

    @NonNull
    @Override
    public Resources getResources() {
        return mContext.getResources();
    }

    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    protected void onDestroyView() {}
}
