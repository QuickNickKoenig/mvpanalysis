package ru.sberbank.mobile.core.log;

import android.util.Log;

import ru.sberbank.mobile.core.BuildConfig;

/**
 * Util class for logging. Use this instead of {@link android.util.Log}.
 */
public final class Logger {

    public static boolean LOG_PRINT_DEBUG = true;
    public static boolean LOG_PRINT_ERROR = true;

    private Logger() {
        throw new AssertionError();
    }

    public static void v(String tag, String message) {
        if (LOG_PRINT_DEBUG) {
            try {
                Log.v(tag, message);
            } catch (Exception ignored) {
            }
        }
    }

    public static void d(String tag, String message, boolean condition) {
        if (condition && LOG_PRINT_DEBUG) {
            try {
                Log.d(tag, message);
            } catch (Exception ignored) {
            }
        }
    }

    public static void d(String tag, String message) {
        if (LOG_PRINT_DEBUG) {
            try {
                Log.d(tag, message);
            } catch (Exception ignored) {
            }
        }
    }

    public static void d(String tag, String message, Throwable throwable) {
        if (LOG_PRINT_DEBUG) {
            try {
                Log.d(tag, message, throwable);
            } catch (Exception ignored) {
            }
        }
    }

    public static void i(String tag, String message) {
        if (LOG_PRINT_DEBUG) {
            try {
                Log.i(tag, message);
            } catch (Exception ignored) {
            }
        }
    }

    public static void w(String tag, String message) {
        if (LOG_PRINT_DEBUG) {
            try {
                Log.w(tag, message);
            } catch (Exception ignored) {
            }
        }
    }

    public static void w(String tag, String message, Throwable throwable) {
        if (LOG_PRINT_DEBUG) {
            try {
                Log.w(tag, message, throwable);
            } catch (Exception ignored) {
            }
        }
    }

    public static void e(String tag, String message) {
        if (LOG_PRINT_ERROR) {
            try {
                Log.e(tag, message);
            } catch (Exception ignored) {
            }
        }
    }

    public static void e(String tag, String message, Throwable throwable) {
        if (LOG_PRINT_ERROR) {
            try {
                Log.e(tag, message, throwable);
            } catch (Exception ignored) {
            }
        }
    }

    public static void wtf(String tag, String message) {
        wtf(tag, message, null);
    }

    public static void wtf(String tag, Throwable throwable) {
        wtf(tag, null, throwable);
    }

    public static void wtf(String tag, String message, Throwable throwable) {
        if (BuildConfig.DEBUG) {
            Log.wtf(tag, message, throwable);
        }
    }

}
