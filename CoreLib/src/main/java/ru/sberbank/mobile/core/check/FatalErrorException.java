package ru.sberbank.mobile.core.check;

/**
 * @author QuickNick
 */
public class FatalErrorException extends Exception {
    public FatalErrorException() {
    }

    public FatalErrorException(String detailMessage) {
        super(detailMessage);
    }

    public FatalErrorException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public FatalErrorException(Throwable throwable) {
        super(throwable);
    }
}
