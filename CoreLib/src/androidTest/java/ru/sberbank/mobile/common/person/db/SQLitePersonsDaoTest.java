package ru.sberbank.mobile.common.person.db;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import ru.sberbank.mobile.common.person.db.SQLitePersonsDao;
import ru.sberbank.mobile.common.person.db.SQLitePersonsHelper;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.CoreEntitiesGenerator;

/**
 * @author QuickNick.
 */
@RunWith(AndroidJUnit4.class)
public class SQLitePersonsDaoTest {

    private static final String DB_NAME = "test_persons.db";
    private SQLitePersonsDao mPersonsDao;

    @Before
    public void setUp() {
        SQLitePersonsHelper helper = new SQLitePersonsHelper(InstrumentationRegistry.getContext(), DB_NAME);
        mPersonsDao = new SQLitePersonsDao(helper);
    }

    @Test
    public void testGetPersons() {
        List<Person> expected = CoreEntitiesGenerator.createPersonsList(false);
        for (Person person : expected) {
            person.id = mPersonsDao.addPerson(person);
        }

        List<Person> actual = mPersonsDao.getPersons();
        assertThat(actual, is(expected));
    }

    @Test
    public void testAddPerson() {
        Person person = CoreEntitiesGenerator.createPerson(false);
        long id = mPersonsDao.addPerson(person);
        assertThat(id > 0, is(true));
    }

    @Test
    public void testUpdatePerson() {
        Person person = CoreEntitiesGenerator.createPerson(false);
        person.id = mPersonsDao.addPerson(person);

        CoreEntitiesGenerator.updatePerson(person);
        int updatedRows = mPersonsDao.updatePerson(person);
        assertThat(updatedRows, is(1));
    }

    @Test
    public void testDeletePerson() {
        Person person = CoreEntitiesGenerator.createPerson(false);
        person.id = mPersonsDao.addPerson(person);

        int deletedRows = mPersonsDao.deletePerson(person);
        assertThat(deletedRows, is(1));
    }

    @After
    public void tearDown() {
        InstrumentationRegistry.getContext().deleteDatabase(DB_NAME);
    }
}
