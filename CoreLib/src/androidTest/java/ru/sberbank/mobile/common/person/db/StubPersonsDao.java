package ru.sberbank.mobile.common.person.db;

import java.util.ArrayList;
import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;

/**
 * @author QuickNick.
 */
public class StubPersonsDao implements IPersonsDao {

    private final List<Person> mPersons;
    private long mLastAddedId = 0;

    public StubPersonsDao() {
        mPersons = new ArrayList<>();
    }

    @Override
    public List<Person> getPersons() {
        return mPersons;
    }

    @Override
    public long addPerson(Person person) {
        return addPersonForTest(person);
    }

    @Override
    public int updatePerson(Person person) {
        int updatedRows = 0;
        int index = getPersonIndex(person);
        if (index >= 0) {
            mPersons.set(index, person);
            updatedRows++;
        }
        return updatedRows;
    }

    @Override
    public int deletePerson(Person person) {
        int deletedRows = 0;
        int index = getPersonIndex(person);
        if (index >= 0) {
            mPersons.remove(index);
            deletedRows++;
        }
        return deletedRows;
    }

    public long addPersonForTest(Person person) {
        mLastAddedId++;
        person.id = mLastAddedId;
        mPersons.add(person);
        return mLastAddedId;
    }

    public void addPersonsForTest(List<Person> persons) {
        for (Person person : persons) {
            addPersonForTest(person);
        }
    }

    private int getPersonIndex(Person person) {
        int index = -1;
        int size = mPersons.size();
        for (int i = 0; i < size; i++) {
            if (mPersons.get(i).id == person.id) {
                index = i;
                break;
            }
        }
        return index;
    }
}
