package ru.sberbank.mobile.core.async;

import android.test.AndroidTestCase;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * <p>Тест простейшего класса, фактически просто для покрытия кода</p>
 *
 * @author sokol @ 27.07.16
 */
public class ContentTest extends AndroidTestCase {

    @Test
    public void testGet() throws Exception {

        Content<Integer> integerContent = new Content<>(42);
        assertEquals((int) integerContent.get(), 42);

        Content<String> emptyContent = new Content<>(null);
        assertNull(emptyContent.get());

        Object something = new Object();
        Content<Object> one = new Content<>(something);
        Content<Object> two = new Content<>(something);

        assertSame(one.get(), two.get());

        // Тут же equals/hashCode
        Content<String> firstFoo = new Content<>("foo");
        Content<String> secondFoo = new Content<>("foo");
        Content<String> firstBar = new Content<>("bar");
        Content<String> secondBar = new Content<>("bar");
        Content<String> anotherEmpty = new Content<>(null);

        assertEquals(firstFoo, secondFoo);
        assertEquals(firstBar, secondBar);
        assertEquals(emptyContent, anotherEmpty);

        assertNotEquals(firstFoo, firstBar);
        assertNotEquals(firstBar, firstFoo);
        assertNotEquals(firstFoo, emptyContent);
        assertNotEquals(firstFoo, integerContent);
    }
}