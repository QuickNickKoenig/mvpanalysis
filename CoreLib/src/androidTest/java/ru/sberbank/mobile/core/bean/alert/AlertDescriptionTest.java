package ru.sberbank.mobile.core.bean.alert;

import android.os.Parcel;

import junit.framework.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


import ru.sberbank.mobile.core.alert.AlertDescription;
import ru.sberbank.mobile.core.CoreEntitiesGenerator;
/**
 * @author QuickNick
 */
public class AlertDescriptionTest extends TestCase {

    public void testParcelability() {
        Parcel parcel = Parcel.obtain();
        try {
            AlertDescription expected = CoreEntitiesGenerator.createAlertDescription();
            parcel.writeParcelable(expected, 0);

            parcel.setDataPosition(0);
            AlertDescription actual = parcel.readParcelable(getClass().getClassLoader());
            assertThat(actual, is(expected));
        } finally {
            parcel.recycle();
        }
    }
}
