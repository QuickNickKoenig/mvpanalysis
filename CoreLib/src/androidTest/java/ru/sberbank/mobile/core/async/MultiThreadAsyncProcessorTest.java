package ru.sberbank.mobile.core.async;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.test.AndroidTestCase;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;


/**
 * @author QuickNick
 */
public class MultiThreadAsyncProcessorTest extends AndroidTestCase {

    private static final Uri TEST_URI = Uri.parse("sberbank://test_uri");

    private MultiThreadAsyncProcessor mMultiThreadAsyncProcessor;
    private HandlerThread mHandlerThread;
    private ObserverImpl mObserver;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mMultiThreadAsyncProcessor = new MultiThreadAsyncProcessor(getContext());

        mHandlerThread = new HandlerThread("TestThread");
        mHandlerThread.start();
        Handler handler = new Handler(mHandlerThread.getLooper());
        mObserver = new ObserverImpl(handler);
        getContext().getContentResolver().registerContentObserver(TEST_URI, true, mObserver);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        mHandlerThread.quit();
    }

    @Test
    public void testSubmit() {
        CallableImpl callable = new CallableImpl();
        PendingResult<Integer> pending = mMultiThreadAsyncProcessor.submit(TEST_URI, callable, false);
        assertThat(pending.getResult(), nullValue());
        assertThat(pending.awaitAndGetResult(), is(1));
        assertThat(pending.getResult(), is(1));
        assertThat(callable.counter, is(1));
        SystemClock.sleep(1000);
        assertThat(mObserver.counter, is(1));
        assertThat(mObserver.uri, is(TEST_URI));
    }

    @Test
    public void testSubmitAndThrottle() {
        CallableImpl callable = new CallableImpl();
        PendingResult<Integer> pending1 = mMultiThreadAsyncProcessor.submit(TEST_URI, callable, false);
        PendingResult<Integer> pending2 = mMultiThreadAsyncProcessor.submit(TEST_URI, callable, false);
        assertThat(true, is(pending1 == pending2));
        assertThat(pending1.getResult(), nullValue());
        assertThat(pending1.awaitAndGetResult(), is(1));
        assertThat(pending1.getResult(), is(1));
        assertThat(callable.counter, is(1));
        SystemClock.sleep(1000);
        assertThat(mObserver.counter, is(1));
        assertThat(mObserver.uri, is(TEST_URI));
    }

    @Test
    public void testSubmitWithForceReplace() {
        CallableImpl callable = new CallableImpl();
        PendingResult<Integer> pending1 = mMultiThreadAsyncProcessor.submit(TEST_URI, callable, false);
        PendingResult<Integer> pending2 = mMultiThreadAsyncProcessor.submit(TEST_URI, callable, true);
        assertThat(false, is(pending1 == pending2));
        pending1.awaitAndGetResult();
        pending2.awaitAndGetResult();
        assertThat(callable.counter, is(2));
    }

    @Test
    public void testGetTasksUri() {
        mMultiThreadAsyncProcessor.submit(Uri.parse("android-app://ru.sberbank/test"), new EmptyCallableImpl(), false);
        List<Uri> filtered = mMultiThreadAsyncProcessor.getTasksUris(TaskFilter.all());
        assertTrue(filtered != null && filtered.size() > 0);
    }

    private static class CallableImpl implements NoExceptionCallable<Integer> {

        public int counter;

        @Override
        public synchronized Integer call() {
            SystemClock.sleep(2000);
            counter++;
            return counter;
        }
    }

    private static class EmptyCallableImpl implements NoExceptionCallable<Object> {
        @Override
        public Object call() {
            return null;
        }
    }

    private static class ObserverImpl extends ContentObserver {

        public Uri uri;
        public int counter;

        /**
         * Creates a content observer.
         *
         * @param handler The handler to run {@link #onChange} on, or null if none.
         */
        public ObserverImpl(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            counter++;
            this.uri = uri;
        }
    }
}
