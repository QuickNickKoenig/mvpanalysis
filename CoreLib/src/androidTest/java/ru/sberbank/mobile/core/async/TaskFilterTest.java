package ru.sberbank.mobile.core.async;

import android.net.Uri;
import android.test.AndroidTestCase;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * <p></p>
 *
 * @author Дмитрий Соколов
 * @since 11.08.16
 */
public class TaskFilterTest extends AndroidTestCase {

    private Uri mUri = Uri.parse("android-app://ru.sberbankmobile/foo");
    private Uri mNestedUri = Uri.parse("android-app://ru.sberbankmobile/foo/bar");
    private Uri mNotNestedUri = Uri.parse("android-app://ru.sberbankmobile/cat/dog");

    private NoExceptionCallable<Integer> mEmptyCallable = new NoExceptionCallable<Integer>() {

        private int mCounter = 0;

        @Override
        public Integer call() {
            mCounter++;
            return mCounter;
        }
    };

    private Map<Uri, PendingResult<Integer>> mAllResults = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        StubPendingResult<Integer> readyResult = new StubPendingResult<>(mUri, mEmptyCallable);
        StubPendingResult<Integer> notReadyNestedResult = new StubPendingResult<>(mNestedUri, mEmptyCallable);
        notReadyNestedResult.overrideIsLoading(true);
        StubPendingResult<Integer> readyNotNestedResult = new StubPendingResult<>(mNotNestedUri, mEmptyCallable);

        mAllResults.put(mUri, readyResult);
        mAllResults.put(mNestedUri, notReadyNestedResult);
        mAllResults.put(mNotNestedUri, readyNotNestedResult);
    }

    @Test
    public void testFilter() throws Exception {

    }

    @Test
    public void testAll() throws Exception {
        TaskFilter filter = TaskFilter.all();

        for (PendingResult<Integer> result: mAllResults.values()) {
            assertTrue(filter.filter(result.getUri(), result));
        }
    }

    @Test
    public void testWithParentUri() throws Exception {
        TaskFilter filter = TaskFilter.withParentUri(mUri);
        int resultsCount = 0;

        for (PendingResult<Integer> result: mAllResults.values()) {
            if (filter.filter(result.getUri(), result)) {
                resultsCount++;
            }
        }

        // Должен получиться 1 nested uri
        assertEquals(1, resultsCount);

        filter = TaskFilter.withParentUri(mNotNestedUri);
        resultsCount = 0;

        for (PendingResult<Integer> result: mAllResults.values()) {
            if (filter.filter(result.getUri(), result)) {
                resultsCount++;
            }
        }

        // Должен получиться 0
        assertEquals(0, resultsCount);
    }

    @Test
    public void testReady() throws Exception {
        TaskFilter filter = TaskFilter.ready();
        int resultsCount = 0;

        for (PendingResult<Integer> result: mAllResults.values()) {
            if (filter.filter(result.getUri(), result)) {
                resultsCount++;
            }
        }

        // Должно получиться 2 ready
        assertEquals(2, resultsCount);
    }

    @Test
    public void testAnd() throws Exception {
        TaskFilter filter = TaskFilter.ready().and(TaskFilter.withParentUri(mUri));
        int resultsCount = 0;

        for (PendingResult<Integer> result: mAllResults.values()) {
            if (filter.filter(result.getUri(), result)) {
                resultsCount++;
            }
        }

        // Должно получиться 0, таких нет
        assertEquals(0, resultsCount);

        // and all
        try {
            TaskFilter.ready().and(TaskFilter.all());
            assertTrue(false);
        } catch (UnsupportedOperationException okay) {}

        try {
            TaskFilter.all().and(TaskFilter.ready());
            assertTrue(false);
        } catch (UnsupportedOperationException okay) {}
    }

    @Test
    public void testOr() throws Exception {
        TaskFilter filter = TaskFilter.ready().or(TaskFilter.withParentUri(mUri));
        int resultsCount = 0;

        for (PendingResult<Integer> result: mAllResults.values()) {
            if (filter.filter(result.getUri(), result)) {
                resultsCount++;
            }
        }

        // Должно получиться все три: они или ready, или с parent uri
        assertEquals(3, resultsCount);

        // or all
        try {
            TaskFilter.ready().or(TaskFilter.all());
            assertTrue(false);
        } catch (UnsupportedOperationException okay) {}

        try {
            TaskFilter.all().or(TaskFilter.ready());
            assertTrue(false);
        } catch (UnsupportedOperationException okay) {}
    }

    @Test
    public void testNot() throws Exception {
        TaskFilter filter = TaskFilter.not(TaskFilter.ready());
        int resultsCount = 0;

        for (PendingResult<Integer> result: mAllResults.values()) {
            if (filter.filter(result.getUri(), result)) {
                resultsCount++;
            }
        }

        // Должно получиться 1 not ready
        assertEquals(1, resultsCount);
    }
}