package ru.sberbank.mobile.viewdispatcher.rates;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ConverterUtils;
import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.check.SimpleResultChecker;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.UnconditionalStatusedEntityWatcher;
import ru.sberbank.mobile.core.view.ViewDispatcherUtils;
import ru.sberbank.mobile.viewdispatcher.R;
import ru.sberbank.mobile.viewdispatcher.app.BaseActivity;
import ru.sberbank.mobile.viewdispatcher.di.ViewDispatcherSampleAppComponent;

/**
 * @author QuickNick.
 */
public class ConverterActivity extends BaseActivity implements IConverterViewDispatcher.Listener {

    @Inject
    ICurrencyRateManager mCurrencyRateManager;

    private IConverterViewDispatcher mConverterViewDispatcher;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ConverterActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDispatcherSampleAppComponent component = getComponent();
        component.inject(this);

        mConverterViewDispatcher = new DefaultConverterViewDispatcher(this, getSupportFragmentManager(), this);
        ViewDispatcherUtils.setContentView(this, savedInstanceState, mConverterViewDispatcher);
        setSupportActionBar(mConverterViewDispatcher.getToolbar());

        getWatcherBundle().add(new RatesBundleWatcher());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mConverterViewDispatcher.destroyView();
    }

    // IConverterViewDispatcher.Listener >>>

    @Override
    public void onConvertClick(IConverterViewDispatcher sender) {
        BigDecimal sourceAmount = mConverterViewDispatcher.getSourceAmount();
        Rate sourceRate = mConverterViewDispatcher.getSourceRate();
        Rate targetRate = mConverterViewDispatcher.getTargetRate();
        if (sourceAmount == null) {
            mConverterViewDispatcher.setSourceAmountError(getString(R.string.source_amount_error));
        } else {
            mConverterViewDispatcher.setSourceAmountError(null);
            if (sourceRate.getCurrency() == targetRate.getCurrency()) {
                mConverterViewDispatcher.showSameRateError();
            } else {
                convert(sourceAmount, sourceRate, targetRate);
            }
        }
    }

    // <<< IConverterViewDispatcher.Listener

    private void convert(BigDecimal sourceAmount, Rate sourceRate, Rate targetRate) {
        BigDecimal targetAmount = ConverterUtils.convert(sourceAmount, sourceRate, targetRate);
        mConverterViewDispatcher.setTargetAmount(targetAmount);
    }

    private class RatesBundleWatcher extends UnconditionalStatusedEntityWatcher<RatesBundle> {

        public RatesBundleWatcher() {
            super(ConverterActivity.this, new SimpleResultChecker(mConverterViewDispatcher));
        }

        @Override
        protected PendingResult<RatesBundle> obtain(boolean force) {
            return mCurrencyRateManager.getRatesBundle();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            mConverterViewDispatcher.setLoading(isLoading);
        }

        @Override
        protected void onSuccess(IContentWatcher watcher, RatesBundle result, boolean isAlertsAreShowing) {
            List<Rate> rates = result.getRates();
            mConverterViewDispatcher.setRates(rates);
        }

        @Override
        protected void onFailureAndAlertsDismissed(IContentWatcher watcher, RatesBundle result) {
            finish();
        }
    }
}
