package ru.sberbank.mobile.viewdispatcher.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.sberbank.mobile.common.di.CoreLibComponent;
import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.viewdispatcher.app.BaseActivity;
import ru.sberbank.mobile.viewdispatcher.person.EditOrCreatePersonActivity;
import ru.sberbank.mobile.viewdispatcher.person.PersonsListActivity;
import ru.sberbank.mobile.viewdispatcher.rates.ConverterActivity;
import ru.sberbank.mobile.viewdispatcher.rates.RatesActivity;

/**
 * @author QuickNick.
 */
@Singleton
@Component(modules = {BaseModelsModule.class, CoreLibModule.class})
public interface ViewDispatcherSampleAppComponent extends CoreLibComponent {

    void inject(BaseActivity activity);

    void inject(RatesActivity activity);

    void inject(ConverterActivity activity);

    void inject(PersonsListActivity activity);

    void inject(EditOrCreatePersonActivity activity);
}
