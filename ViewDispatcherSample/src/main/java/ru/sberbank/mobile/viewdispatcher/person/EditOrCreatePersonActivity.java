package ru.sberbank.mobile.viewdispatcher.person;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.PersonUris;
import ru.sberbank.mobile.common.person.entity.AddPersonResponse;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.check.SimpleResultChecker;
import ru.sberbank.mobile.core.observ.ConditionalStatusedEntityWatcher;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.IContentWatcherCreator;
import ru.sberbank.mobile.core.observ.SimpleContentWatcher;
import ru.sberbank.mobile.core.view.ViewDispatcherUtils;
import ru.sberbank.mobile.viewdispatcher.R;
import ru.sberbank.mobile.viewdispatcher.app.BaseActivity;
import ru.sberbank.mobile.viewdispatcher.di.ViewDispatcherSampleAppComponent;

/**
 * @author QuickNick.
 */
public class EditOrCreatePersonActivity extends BaseActivity implements IEditOrCreatePersonViewDispatcher.Listener {

    private static final String EXTRA_PERSON = "ru.sberbank.mobile.intent.extra.PERSON";

    @Inject
    IPersonsManager mPersonsManager;
    private Person mPerson;

    private IEditOrCreatePersonViewDispatcher mViewDispatcher;

    public static Intent newIntent(Context context, @NonNull Person person) {
        Intent intent = new Intent(context, EditOrCreatePersonActivity.class);
        intent.putExtra(EXTRA_PERSON, person);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDispatcherSampleAppComponent component = getComponent();
        component.inject(this);
        mPerson = (Person) getIntent().getSerializableExtra(EXTRA_PERSON);

        mViewDispatcher = new DefaultEditOrCreatePersonViewDispatcher(this, getSupportFragmentManager(), this);
        ViewDispatcherUtils.setContentView(this, savedInstanceState, mViewDispatcher);
        setSupportActionBar(mViewDispatcher.getToolbar());

        if (savedInstanceState == null) {
            mViewDispatcher.setName(mPerson.name);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewDispatcher.destroyView();
    }

    // <<< Activity lifecycle

    // BaseActivity >>>

    @Override
    protected IContentWatcherCreator createContentWatcherCreator() {
        IContentWatcherCreator creator = new IContentWatcherCreator() {
            @Override
            public IContentWatcher create(Uri uri) {
                IContentWatcher watcher = null;
                if (PersonUris.isAddPersonUri(uri)) {
                    PendingResult<AddPersonResponse> result = getPendingResultRetriever().retrieve(uri);
                    watcher = new AddPersonWatcher(result);
                } else if (PersonUris.isUpdatePersonUri(uri)) {
                    PendingResult<Integer> result = getPendingResultRetriever().retrieve(uri);
                    watcher = new UpdatePersonWatcher(result);
                }
                return watcher;
            }
        };
        return creator;
    }


    // <<< BaseActivity

    // IEditOrCreatePersonViewDispatcher.Listener >>>

    @Override
    public void onOkClick(IEditOrCreatePersonViewDispatcher sender) {
        String name = mViewDispatcher.getName();
        if (TextUtils.isEmpty(name)) {
            mViewDispatcher.setNameError(getString(R.string.name_error));
        } else {
            mViewDispatcher.setNameError(null);
            mPerson.name = name;
            editOrCreatePerson();
        }
    }


    // <<< IEditOrCreatePersonViewDispatcher.Listener

    private void editOrCreatePerson() {
        if (isUpdatingPerson()) {
            PendingResult<Integer> result = mPersonsManager.updatePerson(mPerson);
            getWatcherBundle().add(new UpdatePersonWatcher(result));
        } else {
            PendingResult<AddPersonResponse> result = mPersonsManager.addPerson(mPerson);
            getWatcherBundle().add(new AddPersonWatcher(result));
        }
    }

    private boolean isUpdatingPerson() {
        return (mPerson.id > 0);
    }

    private class AddPersonWatcher extends ConditionalStatusedEntityWatcher<AddPersonResponse> {

        public AddPersonWatcher(PendingResult<AddPersonResponse> pendingResult) {
            super(EditOrCreatePersonActivity.this, new SimpleResultChecker(mViewDispatcher), pendingResult);
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            mViewDispatcher.setLoading(isLoading);
        }

        @Override
        protected void onSuccess(IContentWatcher watcher, AddPersonResponse result, boolean isAlertsAreShowing) {
            super.onSuccess(watcher, result, isAlertsAreShowing);
            detachResult();
            finish();
        }
    }

    private class UpdatePersonWatcher extends SimpleContentWatcher<Integer> {

        public UpdatePersonWatcher(PendingResult<Integer> result) {
            super(EditOrCreatePersonActivity.this);
            setPendingResult(result);
        }

        @Override
        protected PendingResult<Integer> obtain(boolean force) {
            return getPendingResult();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            mViewDispatcher.setLoading(isLoading);
        }

        @Override
        protected void onLoaded(Integer result) {
            detachResult();
            finish();
        }

        @Override
        public boolean isConditionalContentWatcher() {
            return true;
        }
    }
}
