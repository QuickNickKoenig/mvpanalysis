package ru.sberbank.mobile.viewdispatcher.person;

import android.support.v7.widget.Toolbar;

import ru.sberbank.mobile.core.view.IViewDispatcher;

/**
 * @author QuickNick.
 */
public interface IEditOrCreatePersonViewDispatcher extends IViewDispatcher {

    void setLoading(boolean isLoading);

    String getName();

    void setName(String name);

    void setNameError(CharSequence error);

    Toolbar getToolbar();

    interface Listener {

        void onOkClick(IEditOrCreatePersonViewDispatcher sender);
    }
}
