package ru.sberbank.mobile.viewdispatcher.rates;

import android.support.v7.widget.Toolbar;

import java.math.BigDecimal;
import java.util.List;

import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.core.view.IViewDispatcher;

/**
 * @author QuickNick.
 */
public interface IConverterViewDispatcher extends IViewDispatcher {

    void setLoading(boolean loading);

    void setRates(List<Rate> rates);

    void setSourceAmountError(CharSequence error);

    void showSameRateError();

    void setTargetAmount(BigDecimal targetAmount);

    Toolbar getToolbar();

    BigDecimal getSourceAmount();

    Rate getSourceRate();

    Rate getTargetRate();

    interface Listener {

        void onConvertClick(IConverterViewDispatcher sender);
    }
}
