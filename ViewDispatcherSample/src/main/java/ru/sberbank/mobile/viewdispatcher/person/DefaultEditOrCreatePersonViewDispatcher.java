package ru.sberbank.mobile.viewdispatcher.person;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ru.sberbank.mobile.core.view.BaseViewDispatcher;
import ru.sberbank.mobile.viewdispatcher.R;

/**
 * @author QuickNick.
 */

public class DefaultEditOrCreatePersonViewDispatcher extends BaseViewDispatcher implements IEditOrCreatePersonViewDispatcher {

    private final IEditOrCreatePersonViewDispatcher.Listener mListener;

    private ViewGroup mContentContainer;
    private TextInputLayout mNameInputLayout;
    private EditText mNameEditText;
    private Button mConfirmButton;
    private ProgressBar mProgressBar;

    public DefaultEditOrCreatePersonViewDispatcher(Context context, FragmentManager fragmentManager,
                                                   IEditOrCreatePersonViewDispatcher.Listener listener) {
        super(context, fragmentManager);
        mListener = listener;
    }

    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_or_create_person_activity, container, false);
        mContentContainer = (ViewGroup) view.findViewById(R.id.content_container);
        mNameInputLayout = (TextInputLayout) view.findViewById(R.id.name_input_layout);
        mNameEditText = (EditText) view.findViewById(R.id.name_edit_text);
        mConfirmButton = (Button) view.findViewById(R.id.confirm_button);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mConfirmButton.setOnClickListener(new ConfirmButtonClickListener());
        return view;
    }

    @Override
    protected void onDestroyView() {
        mContentContainer = null;
        mNameInputLayout = null;
        mNameEditText = null;
        mConfirmButton = null;
        mProgressBar = null;
    }

    @Override
    public void setLoading(boolean isLoading) {
        if (isLoading) {
            mContentContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mContentContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public String getName() {
        return mNameEditText.getText().toString();
    }

    @Override
    public void setName(String name) {
        mNameEditText.setText(name);
    }

    @Override
    public void setNameError(CharSequence error) {
        mNameInputLayout.setError(error);
    }

    @Override
    public Toolbar getToolbar() {
        return (Toolbar) mView.findViewById(R.id.toolbar);
    }

    private class ConfirmButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mNameEditText.getWindowToken(), 0);
            mListener.onOkClick(DefaultEditOrCreatePersonViewDispatcher.this);
        }
    }
}
