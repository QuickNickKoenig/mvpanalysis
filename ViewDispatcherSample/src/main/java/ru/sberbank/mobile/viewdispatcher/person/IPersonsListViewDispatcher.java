package ru.sberbank.mobile.viewdispatcher.person;

import android.support.v7.widget.Toolbar;

import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.view.IViewDispatcher;

/**
 * @author QuickNick.
 */

public interface IPersonsListViewDispatcher extends IViewDispatcher {

    void setLoading(boolean loading, boolean firstLaunch);

    void setPersons(List<Person> persons);

    Toolbar getToolbar();

    interface Listener {

        void onRefresh(IPersonsListViewDispatcher sender);

        void onPersonClick(IPersonsListViewDispatcher sender, Person person);
    }
}
