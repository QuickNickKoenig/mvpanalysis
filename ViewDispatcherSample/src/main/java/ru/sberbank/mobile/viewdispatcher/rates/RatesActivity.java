package ru.sberbank.mobile.viewdispatcher.rates;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.check.SimpleResultChecker;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.UnconditionalStatusedEntityWatcher;
import ru.sberbank.mobile.core.view.ViewDispatcherUtils;
import ru.sberbank.mobile.viewdispatcher.R;
import ru.sberbank.mobile.viewdispatcher.app.BaseActivity;
import ru.sberbank.mobile.viewdispatcher.di.ViewDispatcherSampleAppComponent;

/**
 * @author QuickNick.
 */
public class RatesActivity extends BaseActivity implements IRatesViewDispatcher.Listener {

    @Inject
    ICurrencyRateManager mCurrencyRateManager;
    private RatesBundleWatcher mRatesBundleWatcher;

    private IRatesViewDispatcher mRatesViewDispatcher;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, RatesActivity.class);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDispatcherSampleAppComponent component = getComponent();
        component.inject(this);

        mRatesViewDispatcher = new DefaultRatesViewDispatcher(this, getSupportFragmentManager(), this);
        ViewDispatcherUtils.setContentView(this, savedInstanceState, mRatesViewDispatcher);
        setSupportActionBar(mRatesViewDispatcher.getToolbar());

        mRatesBundleWatcher = new RatesBundleWatcher(savedInstanceState == null);
        getWatcherBundle().add(mRatesBundleWatcher);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRatesViewDispatcher.destroyView();
        mRatesBundleWatcher = null;
    }

    // <<< Activity lifecycle

    // Menu >>>

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rates_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if (!handled) {
            if (item.getItemId() == R.id.converter_menu_item) {
                startActivity(ConverterActivity.newIntent(this));
                handled = true;
            }
        }
        return handled;
    }

    // <<< Menu

    // IRatesViewDispatcher.Listener >>>

    @Override
    public void onRefresh(IRatesViewDispatcher sender) {
        mRatesBundleWatcher.refresh();
    }

    // <<< IRatesViewDispatcher.Listener

    private class RatesBundleWatcher extends UnconditionalStatusedEntityWatcher<RatesBundle> {

        private boolean mFirstLaunch;

        public RatesBundleWatcher(boolean first) {
            super(RatesActivity.this, new SimpleResultChecker(mRatesViewDispatcher));
            mFirstLaunch = first;
        }

        @Override
        protected PendingResult<RatesBundle> obtain(boolean force) {
            return mCurrencyRateManager.getRatesBundle();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            mRatesViewDispatcher.setLoading(isLoading, mFirstLaunch);
            if (!isLoading) {
                mFirstLaunch = false;
            }
        }

        @Override
        protected void onSuccess(IContentWatcher watcher, RatesBundle result, boolean isAlertsAreShowing) {
            mRatesViewDispatcher.setRates(result.getRates());
        }

        @Override
        protected void onFailureAndAlertsDismissed(IContentWatcher watcher, RatesBundle result) {
            finish();
        }
    }
}
