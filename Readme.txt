Структура

presentation:
- презентации, объясняющие те или иные архитектурные решения.

CoreLib:
- архитектурный каркас (подпакет core).
- общий для всех сэмплов модельный слой (подпакет common):
-- Получение списка курсов валют и его конвертация.
-- CRUD-операции над персонами (простейший вариант -- у персоны есть только имя).

Собственно сэмплы состоят из:
- Стартовый экран.
- Получение списка курсов валют.
- Конвертация валют.
- Список персон.
- Create/Update персону.

UndividedSample:
- Activity не разбивается на Presenter, Controller и View.

ViewDispatcherSample:
- Activity является Presenter; Controller и View выносятся в подклассы ViewDispatcher.

MosbySample
- Activity является View.

MoxySample
- Activity является View.

-----------
Замечания.

- У нас наиболее проработаны варианты с неразделением Activity либо с выделением View во ViewDispatcher.
Соответственно, решения по прослушке модели в MosbySample и MoxySample следует считать сырыми.
Вероятно, посиди над ними я чуть дольше, придумал бы что поизящней.
- Mosby LCE основывается на допущении, что ошибка будет отображаться в тосте или одиночном TextView.
Причём эта ошибка приходит одной-единственной строкой. Допущение слишком смелое для архитектурного каркаса.
- Moxy. attachView() вызывается в 2 методах: onStart()/onResume(). Выстрелит в ногу, если в нём проинициализировать
поля и, тем более, зарегистрировать их как слушателей.
- Moxy. detachView() вызывается в методах onSaveInstanceState() и onStop().
Неожиданно, что именно в onSaveInstanceState(), а не в onPause().
- По Moxy осталось ощущение, что в сэмпле использовал его не на всю мощь.

-----------
Заметки после собеседований.
- Люди воспринимают App.getInstance() в конструкторе Moxy-Presenter (из официального сэмпла)
не как незначительную деталь, вообще не имеющую отношения к делу, а как руководство к действию, что только так и надо делать.
- Люди зачастую в Presenter засовывают сочный слой бизнес-логики (т.е. смешивая его с моделью): асинхронку,
работу с сетью (WTF?).

-----------
Что хотелось бы ещё сделать.

- Составить список кейсов, актуальных в Сбербанк-Онлайн, по которым мы можем объективно оценивать качество
архитектуры.
- Дополнительно поресёрчить Moxy, проверить, что из него можно ещё выжать (особенно по ViewState).
- Сэмпл с Android DataBinding.