package ru.sberbank.mobile.undivided.rates;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.common.rate.ui.RatesAdapter;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.check.SimpleResultChecker2;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.UnconditionalStatusedEntityWatcher;
import ru.sberbank.mobile.undivided.R;
import ru.sberbank.mobile.undivided.app.BaseActivity;
import ru.sberbank.mobile.undivided.di.UndividedSampleAppComponent;

/**
 * @author QuickNick.
 */
public class RatesActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @Inject
    ICurrencyRateManager mCurrencyRateManager;
    private RatesBundleWatcher mRatesBundleWatcher;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private RatesAdapter mAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, RatesActivity.class);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UndividedSampleAppComponent component = getComponent();
        component.inject(this);

        setContentView(R.layout.recycler_view_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mAdapter = new RatesAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mRatesBundleWatcher = new RatesBundleWatcher(savedInstanceState == null);
        getWatcherBundle().add(mRatesBundleWatcher);

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSwipeRefreshLayout = null;
        mRecyclerView = null;
        mProgressBar = null;
        mAdapter = null;
        mRatesBundleWatcher = null;
    }

    // <<< Activity lifecycle

    // Menu >>>

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rates_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if (!handled) {
            if (item.getItemId() == R.id.converter_menu_item) {
                startActivity(ConverterActivity.newIntent(this));
                handled = true;
            }
        }
        return handled;
    }

    // <<< Menu

    // SwipeRefreshLayout.OnRefreshListener >>>

    @Override
    public void onRefresh() {
        mRatesBundleWatcher.refresh();
    }

    // <<< SwipeRefreshLayout.OnRefreshListener

    private class RatesBundleWatcher extends UnconditionalStatusedEntityWatcher<RatesBundle> {

        private boolean mFirstLaunch;

        public RatesBundleWatcher(boolean first) {
            super(RatesActivity.this,
                    new SimpleResultChecker2(RatesActivity.this, getSupportFragmentManager()));
            mFirstLaunch = first;
        }

        @Override
        protected PendingResult<RatesBundle> obtain(boolean force) {
            return mCurrencyRateManager.getRatesBundle();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            mSwipeRefreshLayout.setRefreshing(isLoading);
            if (isLoading) {
                if (mFirstLaunch) {
                    mSwipeRefreshLayout.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                }
            } else {
                mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                mFirstLaunch = false;
            }
        }

        @Override
        protected void onSuccess(IContentWatcher watcher, RatesBundle result, boolean isAlertsAreShowing) {
            mAdapter.setRates(result.getRates());
        }

        @Override
        protected void onFailureAndAlertsDismissed(IContentWatcher watcher, RatesBundle result) {
            finish();
        }
    }
}
