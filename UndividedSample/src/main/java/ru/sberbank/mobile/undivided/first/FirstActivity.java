package ru.sberbank.mobile.undivided.first;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import ru.sberbank.mobile.undivided.R;
import ru.sberbank.mobile.undivided.app.BaseActivity;
import ru.sberbank.mobile.undivided.person.PersonsListActivity;
import ru.sberbank.mobile.undivided.rates.RatesActivity;

/**
 * @author QuickNick.
 */
public class FirstActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        findViewById(R.id.currencies_button).setOnClickListener(new ButtonClickListener(RatesActivity.newIntent(this)));
        findViewById(R.id.persons_button).setOnClickListener(new ButtonClickListener(PersonsListActivity.newIntent(this)));
    }

    private class ButtonClickListener implements View.OnClickListener {

        private final Intent mIntent;

        private ButtonClickListener(Intent intent) {
            mIntent = intent;
        }

        @Override
        public void onClick(View v) {
            startActivity(mIntent);
        }
    }
}
