package ru.sberbank.mobile.undivided.app;

import android.app.Application;

import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.core.di.IHasComponent;
import ru.sberbank.mobile.undivided.di.DaggerUndividedSampleAppComponent;
import ru.sberbank.mobile.undivided.di.UndividedSampleAppComponent;

/**
 * @author QuickNick.
 */
public class UndividedSampleApplication extends Application implements IHasComponent<UndividedSampleAppComponent> {

    private UndividedSampleAppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerUndividedSampleAppComponent.builder()
                .baseModelsModule(new BaseModelsModule(this))
                .coreLibModule(new CoreLibModule(this))
                .build();
    }

    @Override
    public UndividedSampleAppComponent getComponent() {
        return mComponent;
    }
}
