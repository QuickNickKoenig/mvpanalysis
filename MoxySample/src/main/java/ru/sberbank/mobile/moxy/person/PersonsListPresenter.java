package ru.sberbank.mobile.moxy.person;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.PersonUris;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.moxy.di.MoxySampleAppComponent;

/**
 * @author QuickNick
 */
@InjectViewState
public class PersonsListPresenter extends MvpPresenter<IPersonsListView> {

    @Inject
    IPersonsManager mPersonsManager;
    @Inject
    Context mApplicationContext;

    private ISbolObserverListener mPersonsListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData(false);
        }
    };
    private SbolContentObserver mContentObserver = new SbolContentObserver(mPersonsListener);

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        mApplicationContext.getContentResolver().registerContentObserver(PersonUris.PERSONS_URI,
                false, mContentObserver);
    }

    @Override
    public void destroyView(IPersonsListView view) {
        super.destroyView(view);
        mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    public void init(MoxySampleAppComponent component) {
        component.inject(this);
    }

    public void loadPersons(boolean pullToRefresh) {
        checkData(pullToRefresh);
    }

    private void checkData(boolean pullToRefresh) {
        PendingResult<List<Person>> result = mPersonsManager.getPersons();
        if (pullToRefresh) {
            result.markAsDirty();
        }
        boolean loading = result.isLoading();
        getViewState().setLoading(loading);
        if (!result.isLoading()) {
            List<Person> persons = result.getResult();
            getViewState().setPersons(persons);
        }
    }
}
