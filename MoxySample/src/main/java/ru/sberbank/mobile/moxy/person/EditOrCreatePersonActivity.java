package ru.sberbank.mobile.moxy.person;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.moxy.R;
import ru.sberbank.mobile.moxy.app.BaseActivity;
import ru.sberbank.mobile.moxy.di.MoxySampleAppComponent;

/**
 * @author QuickNick.
 */
public class EditOrCreatePersonActivity extends BaseActivity
        implements IEditOrCreatePersonView {

    private static final String EXTRA_PERSON = "ru.sberbank.mobile.intent.extra.PERSON";

    @InjectPresenter(type = PresenterType.WEAK)
    EditOrCreatePersonPresenter mEditOrCreatePersonPresenter;

    private ViewGroup mContentContainer;
    private TextInputLayout mNameInputLayout;
    private EditText mNameEditText;
    private Button mConfirmButton;
    private ProgressBar mProgressBar;

    public static Intent newIntent(Context context, @NonNull Person person) {
        Intent intent = new Intent(context, EditOrCreatePersonActivity.class);
        intent.putExtra(EXTRA_PERSON, person);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEditOrCreatePersonPresenter.init((MoxySampleAppComponent) getComponent(),
                (Person) getIntent().getSerializableExtra(EXTRA_PERSON));

        setContentView(R.layout.edit_or_create_person_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle(mEditOrCreatePersonPresenter.isUpdatingPerson() ? R.string.update_person : R.string.create_person);
        mContentContainer = (ViewGroup) findViewById(R.id.content_container);
        mNameInputLayout = (TextInputLayout) findViewById(R.id.name_input_layout);
        mNameEditText = (EditText) findViewById(R.id.name_edit_text);
        mConfirmButton = (Button) findViewById(R.id.confirm_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (savedInstanceState == null) {
            mNameEditText.setText(mEditOrCreatePersonPresenter.getPerson().name);
        }
        ;
        mConfirmButton.setOnClickListener(new ConfirmButtonClickListener());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContentContainer = null;
        mNameInputLayout = null;
        mNameEditText = null;
        mConfirmButton = null;
        mProgressBar = null;
    }

    // <<< Activity lifecycle

    @Override
    public void setLoading(boolean loading) {
        if (loading) {
            mContentContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mContentContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setNameError(boolean error) {
        mNameInputLayout.setError(error ? getString(R.string.name_error) : null);
    }

    @Override
    public void onSuccess() {
        finish();
    }

    private void tryEditOrCreatePerson() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mNameEditText.getWindowToken(), 0);
        String name = mNameEditText.getText().toString();
        mEditOrCreatePersonPresenter.tryEditOrCreatePerson(name);
    }

    private class ConfirmButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            tryEditOrCreatePerson();
        }
    }
}
