package ru.sberbank.mobile.moxy.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.sberbank.mobile.common.di.CoreLibComponent;
import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.moxy.person.EditOrCreatePersonPresenter;
import ru.sberbank.mobile.moxy.person.PersonsListPresenter;
import ru.sberbank.mobile.moxy.rates.ConverterPresenter;
import ru.sberbank.mobile.moxy.rates.RatesPresenter;

/**
 * @author QuickNick.
 */
@Singleton
@Component(modules = {BaseModelsModule.class, CoreLibModule.class})
public interface MoxySampleAppComponent extends CoreLibComponent {

    void inject(RatesPresenter presenter);

    void inject(ConverterPresenter presenter);

    void inject(PersonsListPresenter presenter);

    void inject(EditOrCreatePersonPresenter presenter);
}
