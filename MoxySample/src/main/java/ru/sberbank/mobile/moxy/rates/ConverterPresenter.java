package ru.sberbank.mobile.moxy.rates;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.math.BigDecimal;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ConverterUtils;
import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.RateUris;
import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.moxy.di.MoxySampleAppComponent;

/**
 * @author QuickNick
 */
@InjectViewState
public class ConverterPresenter extends MvpPresenter<IConverterView> {
    @Inject
    ICurrencyRateManager mCurrencyRateManager;
    @Inject
    Context mApplicationContext;

    private ISbolObserverListener mRatesListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData();
        }
    };
    private SbolContentObserver mContentObserver = new SbolContentObserver(mRatesListener);

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        mApplicationContext.getContentResolver().registerContentObserver(RateUris.RATES_BUNDLE_URI,
                false, mContentObserver);
    }

    @Override
    public void destroyView(IConverterView view) {
        super.destroyView(view);
        mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    public void init(MoxySampleAppComponent component) {
        component.inject(this);
    }

    public void loadRates() {
        checkData();
    }

    public void tryConvert(BigDecimal sourceAmount, Rate sourceRate, Rate targetRate) {
        if (sourceAmount == null) {
            getViewState().showSourceAmountError(true);
        } else {
            getViewState().showSourceAmountError(false);
            if (sourceRate.getCurrency() == targetRate.getCurrency()) {
                getViewState().showSameRatesError();
            } else {
                convert(sourceAmount, sourceRate, targetRate);
            }
        }
    }

    private void convert(BigDecimal sourceAmount, Rate sourceRate, Rate targetRate) {
        BigDecimal targetAmount = ConverterUtils.convert(sourceAmount, sourceRate, targetRate);
        getViewState().setTargetAmount(targetAmount);
    }

    private void checkData() {
        PendingResult<RatesBundle> result = mCurrencyRateManager.getRatesBundle();
        boolean loading = result.isLoading();
        getViewState().setLoading(loading);
        if (!result.isLoading()) {
            RatesBundle ratesBundle = result.getResult();
            if (ratesBundle.isSuccess()) {
                getViewState().setRates(ratesBundle.getRates());
            } else {
                getViewState().showServerError(ratesBundle.getConnectorStatus());
                result.detach();
            }
        }
    }
}
