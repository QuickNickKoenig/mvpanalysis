package ru.sberbank.mobile.moxy.rates;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;

import java.math.BigDecimal;
import java.util.List;

import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.ui.RatesSpinnerAdapter;
import ru.sberbank.mobile.core.format.DecimalFormatter;
import ru.sberbank.mobile.core.network.ConnectorStatus;
import ru.sberbank.mobile.core.utils.LocaleUtils;
import ru.sberbank.mobile.moxy.R;
import ru.sberbank.mobile.moxy.app.BaseActivity;
import ru.sberbank.mobile.moxy.di.MoxySampleAppComponent;

/**
 * @author QuickNick.
 */
public class ConverterActivity extends BaseActivity implements IConverterView {

    @InjectPresenter(type = PresenterType.WEAK)
    ConverterPresenter mConverterPresenter;

    private ViewGroup mContentContainer;
    private TextInputLayout mSourceAmountInputLayout;
    private EditText mSourceAmountEditText;
    private Spinner mSourceSpinner;
    private TextView mTargetAmountTextView;
    private Spinner mTargetSpinner;
    private Button mConvertButton;
    private ProgressBar mProgressBar;
    private RatesSpinnerAdapter mSourceAdapter;
    private RatesSpinnerAdapter mTargetAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ConverterActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.converter_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mContentContainer = (ViewGroup) findViewById(R.id.content_container);
        mSourceAmountInputLayout = (TextInputLayout) findViewById(R.id.source_amount_input_layout);
        mSourceAmountEditText = (EditText) findViewById(R.id.source_amount_edit_text);
        mSourceSpinner = (Spinner) findViewById(R.id.source_currency_spinner);
        mTargetAmountTextView = (TextView) findViewById(R.id.target_amount_text_view);
        mTargetSpinner = (Spinner) findViewById(R.id.target_currency_spinner);
        mConvertButton = (Button) findViewById(R.id.convert_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mSourceAdapter = new RatesSpinnerAdapter();
        mTargetAdapter = new RatesSpinnerAdapter();

        mSourceSpinner.setAdapter(mSourceAdapter);
        mTargetSpinner.setAdapter(mTargetAdapter);
        mConvertButton.setOnClickListener(new ConvertButtonClickListener());

        mConverterPresenter.init((MoxySampleAppComponent) getComponent());
        mConverterPresenter.loadRates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContentContainer = null;
        mSourceAmountInputLayout = null;
        mSourceAmountEditText = null;
        mSourceSpinner = null;
        mTargetAmountTextView = null;
        mTargetSpinner = null;
        mConvertButton = null;
        mProgressBar = null;
        mSourceAdapter = null;
        mTargetAdapter = null;
    }

    // IConverterView >>>

    @Override
    public void setLoading(boolean loading) {
        if (loading) {
            mContentContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mContentContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setRates(List<Rate> rates) {
        mSourceAdapter.setRates(rates);
        mTargetAdapter.setRates(rates);
    }

    @Override
    public void showServerError(ConnectorStatus status) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.warning)
                .setMessage(status.getTextResId())
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setCancelable(false).show();
    }

    @Override
    public void setTargetAmount(BigDecimal targetAmount) {
        mTargetAmountTextView.setText(DecimalFormatter.format(targetAmount, LocaleUtils.getEnglishLocale()));
    }

    @Override
    public void showSourceAmountError(boolean error) {
        mSourceAmountInputLayout.setError(error ?
                getString(R.string.source_amount_error) : null);
    }

    @Override
    public void showSameRatesError() {
        Toast.makeText(this, R.string.currencies_must_be_different, Toast.LENGTH_LONG).show();
    }

    // <<< IConverterView

    private BigDecimal getSourceAmount() {
        BigDecimal decimal = DecimalFormatter.parseBigDecimal(mSourceAmountEditText.getText().toString(),
                LocaleUtils.getEnglishLocale());
        return decimal;
    }

    private Rate getRateFromSpinner(Spinner spinner) {
        int selectedIndex = spinner.getSelectedItemPosition();
        Rate rate = (Rate) spinner.getAdapter().getItem(selectedIndex);
        return rate;
    }

    private class ConvertButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            mConverterPresenter.tryConvert(getSourceAmount(), getRateFromSpinner(mSourceSpinner),
                    getRateFromSpinner(mTargetSpinner));
        }
    }
}
