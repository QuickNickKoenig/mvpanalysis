package ru.sberbank.mobile.moxy.app;

import android.app.Application;

import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.core.di.IHasComponent;
import ru.sberbank.mobile.moxy.di.DaggerMoxySampleAppComponent;
import ru.sberbank.mobile.moxy.di.MoxySampleAppComponent;

/**
 * @author QuickNick.
 */
public class MoxySampleApplication extends Application implements IHasComponent<MoxySampleAppComponent> {

    private MoxySampleAppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerMoxySampleAppComponent.builder()
                .baseModelsModule(new BaseModelsModule(this))
                .coreLibModule(new CoreLibModule(this))
                .build();
    }

    @Override
    public MoxySampleAppComponent getComponent() {
        return mComponent;
    }
}
