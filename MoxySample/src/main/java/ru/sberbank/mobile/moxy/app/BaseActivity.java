package ru.sberbank.mobile.moxy.app;

import com.arellomobile.mvp.MvpAppCompatActivity;

import ru.sberbank.mobile.core.di.IHasComponent;

/**
 * @author QuickNick
 */
public class BaseActivity extends MvpAppCompatActivity {

    protected <T> T getComponent() {
        IHasComponent<T> hasComponent = (IHasComponent<T>) getApplication();
        return hasComponent.getComponent();
    }
}
