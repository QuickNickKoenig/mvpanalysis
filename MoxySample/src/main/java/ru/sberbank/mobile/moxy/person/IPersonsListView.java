package ru.sberbank.mobile.moxy.person;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;

/**
 * @author QuickNick
 */
public interface IPersonsListView extends MvpView {

    void setLoading(boolean loading);

    void setPersons(List<Person> persons);
}
